import React, { useEffect , useState }  from 'react';
import { StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';
import { SafeAreaView } from 'react-navigation';
import Spacer from '../components/Spacer';
import { requestPermissionsAsync } from 'expo-location';
import Map from '../components/Map';


const TrackCreateScreen = () => {
    const [err, setErr ] = useState(null);

    const startWatching = async () => {
        try {
            await requestPermissionsAsync();
        } catch (e) {
            setErr(e);
        }
    };

    useEffect(() => {
        startWatching();
    }, []);

    return (
        <SafeAreaView forceInset={{ top: 'always' }}>
            <Spacer>
                <Text h2>Create Screen</Text>
                
            </Spacer>
            <Map />

        </SafeAreaView>
    );
};

const styles = StyleSheet.create({});

export default TrackCreateScreen;

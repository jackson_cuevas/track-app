import React from 'react';
import { Text, StyleSheet } from 'react-native';
import MapView, { Polyline } from 'react-native-maps';

const Map = () => {
    let points = [];
    for (let i = 0; i < 20; i++) {
        if (i % 2 === 0) {
            points.push({
                latitude: 18.426880 + i * 0.001,
                longitude: -70.008716 + i * 0.001
            });
        } else {
            points.push({
                latitude: 18.426880 - i * 0.002,
                longitude: -70.008716 - i * 0.001
            });
        }
    }

    return <MapView
        style={styles.map}
        initialRegion={{
            latitude: 18.427944,
            longitude: -70.008807,
            latitudeDelta: 0.001,
            longitudeDelta: 0.01
        }}
    >
        <Polyline coordinates={points} />
    </MapView>
};

const styles = StyleSheet.create({
    map: {
        height: 300
    }
});

export default Map; 